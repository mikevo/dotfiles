if status is-login
	source ~/.config/fish/login.fish
end

set -g theme_display_user yes
set -g theme_hide_hostname no
